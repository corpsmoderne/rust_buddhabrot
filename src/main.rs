use std::fs::File;
use std::io::BufWriter;
use std::path::Path;
use std::time::Instant;
use rand::prelude::*;
use rayon::prelude::*;

const F : u32 = 24;
const SIZE : (u32,u32) = (5000, 5000);
const ZOOM : f32 = 1250.0;

//const F : u32 = 4;
//const SIZE : (u32,u32) = (1000, 1000);
//const ZOOM : f32 = 250.0;

#[derive(Clone,Debug)]
struct Pixel {
    r: u32,
    g: u32,
    b: u32
}

type Frame = Vec<Pixel>;

fn mandel(x0: f32, y0: f32, w: u32, h:u32, zoom: f32, iter_max: u32)
          -> Option<(u32, Vec<(u32,u32)>)> {
    let mut x : f32 = 0.0;
    let mut y : f32 = 0.0;
    let mut i : u32 = 0;
    let mut pts : Vec<(u32, u32)> = Vec::new();
    let w0 = (w/2) as f32;
    let h0 = (h/2) as f32;

    while i < iter_max {
        let x_x = x*x;
        let y_y = y*y;
        if x_x + y_y > 4.0 {
            break;
        }
        y = 2.0 * x * y + y0;
        x = x_x - y_y + x0;
        
        let x2 = (w0 + x * zoom) as i32;
        let y2 = (h0 + y * zoom) as i32;
        if x2 > 0 && y2 > 0 && x2 < w as i32 && y2 < h as i32 {
            pts.push((x2 as u32, y2 as u32));
        }
        i += 1;
    }
    if i < iter_max {
        Some((i, pts))
    } else {
        None
    }
}

fn make_frame(width: u32, height: u32) -> Frame {
    vec![Pixel { r:0, g:0, b:0 } ; (width*height) as usize]
}

fn gen_frame(width: u32, height: u32,
             zoom: f32, n: u32, iter_max: Pixel) -> Frame {
    let mut frame = make_frame(width, height);
    let mut rng = rand::thread_rng();
    let mx = iter_max.r.max(iter_max.g).max(iter_max.b);
    for _ in 0..n {
        let x : f32 = rng.gen::<f32>() * width as f32;
        let y : f32 = rng.gen::<f32>() * height as f32;
        let x0 = (x as f32 - width as f32 / 2.0) / zoom;
        let y0 = (y as f32 - height as f32 / 2.0) / zoom;        
        if let Some((i,lst)) = mandel(x0, y0, width, height, zoom, mx) {
            for (x, y) in &lst {
                if i < iter_max.r {
                    frame[(x + y * width) as usize].r += 1;
                }
                if i < iter_max.g {
                    frame[(x + y * width) as usize].g += 1;
                }
                if i < iter_max.b {
                    frame[(x + y * width) as usize].b += 1;
                }
            }
        }
    }
    frame
}

fn accumulate_frames(frame : &mut Frame, frames : Vec<Frame>) {
    for f in frames {
        for (i, p) in f.iter().enumerate() {
            frame[i].r += p.r;
            frame[i].g += p.g;
            frame[i].b += p.b;
        }
    }
}

fn normalize_frame(frame: &Frame) -> Frame {
    let mx_r = frame.iter().fold(0, | m, i | m.max(i.r));
    let mx_g = frame.iter().fold(0, | m, i | m.max(i.g));
    let mx_b = frame.iter().fold(0, | m, i | m.max(i.b));
    
    frame.iter().map(| p | {
        let r = p.r as f32 / mx_r as f32;
        let g = p.g as f32 / mx_g as f32;
        let b = p.b as f32 / mx_b as f32;
        Pixel { r: (256.0 * r.sqrt()) as u32,
                g: (256.0 * g.sqrt()) as u32,
                b: (256.0 * b.sqrt()) as u32 }
    }).collect()
}

fn write_image(img: Vec<Pixel>,
               width: u32, height: u32,
               filename: &str) {
    let mut buffer = vec![0 as u8 ; img.len() * 3];    
    for (i, c) in img.iter().enumerate() {
        buffer[i*3+0] = c.r.min(255).max(0) as u8;
        buffer[i*3+1] = c.g.min(255).max(0) as u8;
        buffer[i*3+2] = c.b.min(255).max(0) as u8;
    }
    let path = Path::new(filename);
    let file = File::create(path).unwrap();
    let ref mut w = BufWriter::new(file);

    let mut encoder = png::Encoder::new(w, width, height);
    encoder.set_color(png::ColorType::RGB);
    encoder.set_depth(png::BitDepth::Eight);
    let mut writer = encoder.write_header().unwrap();

    writer.write_image_data(&buffer).unwrap();
}

fn main() {
    let threads = rayon::current_num_threads() as u32;
    let n : u32 = 10_000_000;
    let now = Instant::now();
    let mut total : u64 = 0;
    let mut old_time : f32 = 0.0;
    let (width, height) = SIZE;
    let mut frame = make_frame(width, height);
    loop {
        let frames : Vec<Frame> = (0..threads).into_par_iter()
            .map(|_| gen_frame(width, height, ZOOM, n/threads as u32,
                               Pixel { r: F*1024, g: F*256, b: F*64 }))
            .collect();
        accumulate_frames(&mut frame, frames);

        let pixels = normalize_frame(&frame);
        write_image(pixels, width, height, "out.png");
        
        total += n as u64;
        let time = now.elapsed().as_millis() as f32 / 1000.0;
        let dt = time-old_time;
        old_time = time;
        println!("{} million points, in {} seconds [{}]",
                 total/1_000_000, time, dt);
    }
}
